- 👋 Hi, I’m @myHerbDev
- 👀 I’m interested in Google Cloud, Generative AI, App Development learning, Developing new Apps and services regarding enviroment & sustainability.
- 🌱 I’m currently learning Google Developer & Skills boost, Coding, Deploying, GitHub, API, Webhooks, Integrations, Pyton, Digital Marketing and Marketing via API's Feeds.
- 💞️ I’m looking to collaborate on myHerb Project, TerraSight (Maps, Navigation, Weather Data, Climate and sustainability) Project, Integrating AI, Collaborating on Google Cloud, Google Earth. Collaborate to profit as good as it can get and bring myHerb to the next level. 
- 📫 How to reach me: Email: contact@myherb.co.il / aaron@myherb.co.il
- Zendesk: support@myherb.zendesk.com

<!---
myHerbDev/myHerbDev is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
